package net.threetag.threecore.scripts.accessors.icon;

import net.minecraft.util.ResourceLocation;
import net.threetag.threecore.scripts.ScriptParameterName;
import net.threetag.threecore.util.icon.TexturedIcon;

import java.awt.*;

public class TexturedIconAccessor extends IconAccessor<TexturedIcon> {

	public TexturedIconAccessor(TexturedIcon value) {
		super(value);
	}

	public TexturedIconAccessor setTexture(@ScriptParameterName("texture") String texture) {
		return (TexturedIconAccessor) makeAccessor(this.iconValue = new TexturedIcon(new ResourceLocation(texture), this.iconValue.u, this.iconValue.v, this.iconValue.width, this.iconValue.height, this.iconValue.textureWidth, this.iconValue.textureHeight, this.iconValue.tint));
	}

	public String getTexture() {
		return this.iconValue.texture.toString();
	}

	public TexturedIconAccessor setUV(@ScriptParameterName("u") int u, @ScriptParameterName("v") int v) {
		return (TexturedIconAccessor) makeAccessor(this.iconValue = new TexturedIcon(this.iconValue.texture, u, v, this.iconValue.width, this.iconValue.height, this.iconValue.textureWidth, this.iconValue.textureHeight, this.iconValue.tint));
	}

	public TexturedIconAccessor setSize(@ScriptParameterName("width") int width, @ScriptParameterName("height") int height) {
		return (TexturedIconAccessor) makeAccessor(this.iconValue = new TexturedIcon(this.iconValue.texture, this.iconValue.u, this.iconValue.v, width, height, this.iconValue.textureWidth, this.iconValue.textureHeight, this.iconValue.tint));
	}

	public TexturedIconAccessor setTextureSize(@ScriptParameterName("textureWidth") int textureWidth, @ScriptParameterName("textureHeight") int textureHeight) {
		return (TexturedIconAccessor) makeAccessor(this.iconValue = new TexturedIcon(this.iconValue.texture, this.iconValue.u, this.iconValue.v, this.iconValue.width, this.iconValue.height, textureWidth, textureHeight, this.iconValue.tint));
	}

	public TexturedIconAccessor setTint(@ScriptParameterName("red") int red, @ScriptParameterName("green") int green, @ScriptParameterName("blue") int blue) {
		return (TexturedIconAccessor) makeAccessor(this.iconValue = new TexturedIcon(this.iconValue.texture, this.iconValue.u, this.iconValue.v, this.iconValue.width, this.iconValue.height, this.iconValue.textureWidth, this.iconValue.textureHeight, new Color(red, green, blue)));
	}
}
