package net.threetag.threecore.scripts.accessors.icon;

import net.threetag.threecore.scripts.ScriptParameterName;
import net.threetag.threecore.util.icon.CompoundIcon;
import net.threetag.threecore.util.icon.IIcon;

public class CompoundIconAccessor extends IconAccessor<CompoundIcon> {

	public CompoundIconAccessor(CompoundIcon value) {
		super(value);
	}

	public IconAccessor<?>[] getIcons() {
		IconAccessor<?>[] icons = new IconAccessor[this.value.getIcons().length];

		for (int i = 0; i < icons.length; i++) {
			IIcon icon = this.value.getIcons()[i];
			icons[i] = (IconAccessor<?>) makeAccessor(icon);
		}

		return icons;
	}

	public CompoundIconAccessor setIcons(@ScriptParameterName("iconAccessors") IconAccessor<?>[] iconAccessors) {
		IIcon[] icons = new IIcon[iconAccessors.length];

		for (int i = 0; i < iconAccessors.length; i++) {
			icons[i] = iconAccessors[i].getValue();
		}

		this.iconValue = new CompoundIcon(icons);
		return this;
	}
}
