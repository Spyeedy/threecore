package net.threetag.threecore.scripts.accessors.icon;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;
import net.threetag.threecore.scripts.ScriptParameterName;
import net.threetag.threecore.scripts.accessors.ItemStackAccessor;
import net.threetag.threecore.scripts.accessors.ScriptAccessor;
import net.threetag.threecore.util.icon.ItemIcon;

public class ItemIconAccessor extends IconAccessor<ItemIcon> {

	public ItemIconAccessor(ItemIcon icon) {
		super(icon);
	}

	public ItemStackAccessor getItemStack() {
		return (ItemStackAccessor) ScriptAccessor.makeAccessor(this.iconValue.stack);
	}

	public ItemStackAccessor setItemStack(@ScriptParameterName("itemName") String itemName, @ScriptParameterName("count") int count) {
		Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(itemName));

		if (item != null) {
			ItemStack stack = new ItemStack(item, count);

			this.iconValue = new ItemIcon(stack);

			return (ItemStackAccessor) ScriptAccessor.makeAccessor(stack);
		}

		return ItemStackAccessor.EMPTY;
	}
}
