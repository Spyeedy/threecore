package net.threetag.threecore.scripts.accessors.icon;

import net.threetag.threecore.scripts.accessors.ScriptAccessor;
import net.threetag.threecore.util.icon.IIcon;

public class IconAccessor<T extends IIcon> extends ScriptAccessor<T> {

	public T iconValue;

	public IconAccessor(T value) {
		super(value);
		this.iconValue = value;
	}

	@Override
	public boolean equals(Object o) {
		return this.iconValue.equals(o);
	}

	@Override
	public int hashCode() {
		return this.iconValue.hashCode();
	}

	@Override
	public T getValue() {
		return this.iconValue;
	}
}
