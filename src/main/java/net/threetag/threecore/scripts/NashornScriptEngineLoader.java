package net.threetag.threecore.scripts;

import jdk.nashorn.api.scripting.NashornScriptEngineFactory;

import javax.script.ScriptEngine;

public class NashornScriptEngineLoader {

    public static ScriptEngine getEngine() {
        return new NashornScriptEngineFactory().getScriptEngine();
    }

}
