package net.threetag.threecore.scripts;

import javax.annotation.Nonnull;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class ScriptEngineLoader {

    public static ScriptEngine getEngine() {
        try {
            return NashornScriptEngineLoader.getEngine();
        } catch (@Nonnull final Throwable ignore) {
        }
        return new ScriptEngineManager().getEngineByName("JavaScript");
    }

}
