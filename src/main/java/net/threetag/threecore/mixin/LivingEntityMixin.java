package net.threetag.threecore.mixin;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Pose;
import net.threetag.threecore.util.AsmHooks;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(LivingEntity.class)
public class LivingEntityMixin {

    @ModifyArg(method = "getEyeHeight", index = 1, at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/LivingEntity;getStandingEyeHeight(Lnet/minecraft/entity/Pose;Lnet/minecraft/entity/EntitySize;)F"))
    private EntitySize getEyeHeightProxy(EntitySize size) {
        return size.scale(1.0F / AsmHooks.getHeightScale((Entity) (Object) this));
    }

    @Inject(method = "getEyeHeight", at = @At("RETURN"), cancellable = true)
    private void getEyeHeight(Pose pose, EntitySize size, CallbackInfoReturnable<Float> info) {
        if (pose != Pose.SLEEPING) {
            final float scale = AsmHooks.getHeightScale((Entity) (Object) this);

            if (scale != 1.0F) {
                info.setReturnValue(info.getReturnValueF() * scale);
            }
        }
    }

}
