package net.threetag.threecore.mixin;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.Pose;
import net.threetag.threecore.util.AsmHooks;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Entity.class)
public abstract class EntityMixin {

    @Inject(at = @At("RETURN"), method = "getSize", cancellable = true)
    private void getSize(Pose pose, CallbackInfoReturnable<EntitySize> info) {
        info.setReturnValue(AsmHooks.getOverridenSize(info.getReturnValue(), (Entity) (Object) this, pose));
    }

}
