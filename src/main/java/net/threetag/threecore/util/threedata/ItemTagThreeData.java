package net.threetag.threecore.util.threedata;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.Tags;

public class ItemTagThreeData extends ThreeData<Tags.IOptionalNamedTag<Item>> {

    public ItemTagThreeData(String key) {
        super(key);
    }

    @Override
    public Tags.IOptionalNamedTag<Item> parseValue(JsonObject jsonObject, Tags.IOptionalNamedTag<Item> defaultValue) {
        return ItemTags.createOptional(new ResourceLocation(JSONUtils.getString(jsonObject, this.jsonKey, defaultValue.getName().toString())));
    }

    @Override
    public void writeToNBT(CompoundNBT nbt, Tags.IOptionalNamedTag<Item> value) {
        nbt.putString(this.key, value.getName().toString());
    }

    @Override
    public Tags.IOptionalNamedTag<Item> readFromNBT(CompoundNBT nbt, Tags.IOptionalNamedTag<Item> defaultValue) {
        if (!nbt.contains(this.key))
            return defaultValue;
        return ItemTags.createOptional(new ResourceLocation(nbt.getString(this.key)));
    }

    @Override
    public JsonElement serializeJson(Tags.IOptionalNamedTag<Item> value) {
        return new JsonPrimitive(value.getName().toString());
    }
}
